<?php

return [
	'mode'                 => '',
	'format'               => 'A4',
	'default_font_size'    => '12',
	'default_font'         => 'times-new-roman',
	'margin_left'          => 0,
	'margin_right'         => 0,
	'margin_top'           => 0,
	'margin_bottom'        => 0,
	'margin_header'        => 0,
	'margin_footer'        => 0,
	'orientation'          => 'P',
	'title'                => 'Receipt',
	'author'               => '',
	'display_mode'         => 'fullpage'
];