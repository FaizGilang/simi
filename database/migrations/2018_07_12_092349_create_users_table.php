<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('reset');
            $table->unsignedInteger('occupation');
            $table->rememberToken();
            $table->timestamps();

        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('occupation')->references('id')->on('occupations')->onDelete('cascade');
        });

        DB::table('users')->insert([
            [
                'name' => 'admin',
                'occupation' => '1',
                'email' => 'admin@mail.com',
                'reset' => '0',
                'password' => bcrypt('katakunci')
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
 