<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occupations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('division_id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::table('occupations', function (Blueprint $table) {
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade');
        });
        
          DB::table('occupations')->insert([
            [
                'name' => 'admin',
                'division_id' => '1'
            ],
            [
                'name' => 'Kordinator',
                'division_id' => '2'
            ],
            [
                'name' => 'Wakil Kordinator',
                'division_id' => '2'
            ],
            [
                'name' => 'Kabag Tata Usaha',
                'division_id' => '3'
            ],
            [
                'name' => 'Kasubbag Humas & Kerjasama',
                'division_id' => '3'
            ],
            [
                'name' => 'Kasubbag Akuntansi & Perencanaan Keuangan',
                'division_id' => '3'
            ],
            [
                'name' => 'Kasubbag Kepegawaian & Administrasi Umum',
                'division_id' => '3'
            ],
            [
                'name' => 'Kasubbag Data Informasi & Pengembangan TIK',
                'division_id' => '3'
            ],
            [
                'name' => 'Kassubag Akademik & Kewirausahaan',
                'division_id' => '3'
            ],
            [
                'name' => 'Subbag  Kepegawaian & Administrasi Umum',
                'division_id' => '3'
            ], 
            [
                'name' => 'Kaprodi Desain Grafis',
                'division_id' => '4'
            ],
            [
                'name' => 'Kepala Laboratorium Desain Grafis',
                'division_id' => '4'
            ],
            [
                'name' => 'Dosen Desain Grafis',
                'division_id' => '4'
            ],
            [
                'name' => 'Kaprodi TKPR',
                'division_id' => '5'
            ],
            [
                'name' => 'Kepala Laboratorium TPKR',
                'division_id' => '5'
            ],
            [
                'name' => 'Dosen TPKR',
                'division_id' => '5'
            ],
            [
                'name' => 'Kaprodi Jasa Pariwisata',
                'division_id' => '6'
            ],
            [
                'name' => 'Kepala Laboratorium Jasa Pariwisata',
                'division_id' => '6'
            ],
            [
                'name' => 'Dosen Jasa Pariwisata',
                'division_id' => '6'
            ],
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occupations');
    }
}
