<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id');
            $table->string('number');
            $table->string('fund_source');
            $table->string('year');
            $table->string('deleteyear');
            $table->string('vendor');
            $table->integer('user_id');
            $table->string('arrival_doc');
            $table->string('distribute_doc');
            $table->string('condition');
            $table->string('location');
            $table->string('status');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
