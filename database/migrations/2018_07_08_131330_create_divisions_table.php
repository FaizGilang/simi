<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('divisions')->insert([
            [
                'name' => 'admin'
            ],
            [
                'name' => 'Kordinator'
            ],
            [
                'name' => 'Tata Usaha'
            ],
            [
                'name' => 'Prodi Desain Grafis'
            ],
            [
                'name' => 'Prodi TPKR'
            ],
            [
                'name' => 'Prodi Jasa Pariwisata'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
