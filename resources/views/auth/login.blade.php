@extends('layouts.login')

@section('content')
<div class="container">
    <div>
      <h1 class="text-center huge">Akademi Komunitas Negeri Demak</h1>
       <h4 class="text-center">Menjadikan Akademi Komunitas unggul dan berkelas Nasional tahun 2030</h4>
    </div>
    <div class="col-md-3">
        
    </div>
  <div class="col-md-6">
    <div class="row">


        <div class="col-md-12 ">
            <div class="panel-transparent">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" style="background-color: transparent; color:#fff" required autofocus>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" style="background-color: transparent; color:#fff" required>

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn " style=" background-color: rgba(122, 130, 136, 0.2) !important">
                                    Login
                                </button>

                                <a class="btn btn-link" style=" color: rgba(122, 130, 136, 0.9) !important" href="/user/reset">
                                    Lupa Password anda?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
