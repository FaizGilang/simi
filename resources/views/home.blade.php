@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="material-icons">language</i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Data Keseluruhan Inventaris</h4>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">weekend</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Semua Barang</p>
                            <h3 class="card-title">{{$all}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="rose">
                            <i class="material-icons">equalizer</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Barang Aktif</p>
                            <h3 class="card-title">{{$active}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="green">
                            <i class="material-icons">store</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Barang Dibagikan</p>
                            <h3 class="card-title">{{$dis}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="red">
                               <i class="material-icons">build</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Barang Dihapus</p>
                            <h3 class="card-title">{{$proc}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
