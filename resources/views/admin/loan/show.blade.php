@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-md-12">

            <div class="col-md-4" style="padding:0; margin-top:-30px; ">
                <a class="btn btn-md btn-white" href="{{ url()->current() }}" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px outline: 0; box-shadow: none;">
                    <h4 class="">Detail Penerimaan Barang</h4>
                </a>    
            </div>
                 
            </div>
            </div>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
            <tr>
                <td>Peminjam</td>
                <td>{{$data->name}}</td>
            </tr>

            <tr>
                <td>Status</td>
                <td>{{$data->status}}</td>
            </tr>

        </table>

        <h3>Barang</h3>
        <table class="table table-hover table-bordered">
            <tr>
                <th>No.</th>
                <th>Nama Barang</th>
                <th>Nomor Barang</th>
            </tr>
           @foreach($data1 as $index=>$a)
            <tr>
                <th>{{$index+1}}</th>
                <th>{{$a->name}}</th>
                <th>{{$a->number}}</th>
            </tr>
            @endforeach 
              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
