@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-xs-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-xs-12">

            <div class="col-xs-4" style="padding:0; margin-top:-20px; ">
                <a class="btn btn-lg btn-white" href="/loan" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 0; box-shadow: none;">
                    <h4 class="">Peminjaman Barang</h4>
                </a>
                 
            </div>
            </div>
            </div>
                <div class="card-content">
                      
                                 <div class="material-datatables">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
         <thead class="text-info">
      
                                <tr>
                                    <th>No</th>
                                    <th>Peminjam</th>
                                    <th>Tanggal Pinjam</th>
                                    <th>Tanggal Kembali</th>
                                    <th>Status</th>
                                    <th>Action  <a class="btn btn-info btn-sm" style="margin-left:10px;  margin-top: -5px; margin-bottom: -5px;" href="{{ url('loan/add')}}">
                                            <i class="fa fa-plus"></i>
                                        </a></th>
                                </thead>
                                <tbody>
                                    @foreach($data as $index=>$a)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $a->name }}</td>
                                        <td>{{ Carbon\Carbon::parse($a->borrowdate)->format('j M Y')}}</td>
                                        <td>{{ Carbon\Carbon::parse($a->returndate)->format('j M Y')}}</td>
                                        <td>{{ $a->status }}</td>
                                        <td>
                                            
                                            <a class="btn btn-sm btn-primary button1" href="{{ url('loan/detail/'. $a->id) }}">
                                               <i class="fa fa-info"></i>
                                            </a>
                                        
                                             <a class="btn btn-sm btn-success button1" href="{{ url('loan/verify/'. $a->id) }}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                            
                                           <!--  <a class="btn btn-sm btn-danger button1" href="{{ url('user/' . $a->id . '/delete') }}" onclick="return confirm('tenan ki meh dihapus?')">
                                            <i class="fa fa-trash"></i>
                                            </a> -->
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection