@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Ubah Data Kategori Barang</h4>
            <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}
                <div class="form-group label-floating">
                    <label class="control-label">Nama Kategori</label>
                    <input type="text" name="name" class="form-control " value="{{$data->name}}">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Kode Kategori</label>
                    <input type="text" name="code" class="form-control " value="{{$data->code}}">
                </div>
                <button type="submit" name="save" value="save" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\EditItemCategoryRequest', '#my-form'); !!}
@endsection