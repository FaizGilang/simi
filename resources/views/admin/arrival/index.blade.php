@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-xs-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-xs-12">

            <div class="col-xs-4" style="padding:0; margin-top:-20px; ">
                <a class="btn btn-lg btn-white" href="/arrival" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 0; box-shadow: none;">
                    <h4 class="">Penerimaan Barang</h4>
                </a>
                 
            </div>
            <div class="col-xs-4" style="padding:0; margin-top: -10px;">
                <a class="btn btn-md" href="/distribution" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 1px; box-shadow: none;">
                     <h4 class="">Serah Terima</h4>
                </a>
            </div>
            <div class="col-xs-4" style="padding:0; margin-top: -10px;">
                <a class="btn btn-md" href="/warehouse/delyear" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 1px; box-shadow: none;">
                     <h4 class="">Penghapusan Barang</h4>
                </a>
            </div>
            </div>
            </div>
                <div class="card-content">
                      
                                <div class="material-datatables">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
         <thead class="text-info">
      
                                <tr>
                                    <th>No</th>
                                    <th>Tahun Anggaran</th>
                                    <th>Status</th>
                                    <th>Action  <a class="btn btn-info btn-sm" style="margin-left:10px;  margin-top: -5px; margin-bottom: -5px;" href="{{ url('arrival/add')}}">
                                            <i class="fa fa-plus"></i>
                                        </a></th>
                                </thead>
                                <tbody>
                                    @foreach($data as $index=>$a)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $a->year }}</td>
                                        @if($a->status != "verified")
                                        <td>Menunggu Verifikasi</td>
                                        @else
                                         <td>Telah Diverifikasi</td>
                                         @endif
                                        <td>
                                            
                                            <a class="btn btn-sm btn-primary button1" href="{{ url('arrival/detail/'. $a->id) }}">
                                               <i class="fa fa-info"></i>
                                            </a>
                                            @if($a->status == "unverified")
                                            <a class="btn btn-sm btn-success button1" href="{{ url('arrival/edit/' . $a->id) }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            @endif
                                            <!--  <a class="btn btn-sm btn-rose button1" href="{{ url('arrival/detail/'. $a->id) }}">
                                               <i class="fa fa-pencil"></i>
                                            </a> -->
                                            @if(Auth()->user()->occupation === 7)
                                            @if($a->status != "verified")
                                             <a class="btn btn-sm btn-success button1" href="{{ url('arrival/verify/'. $a->id) }}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                            @endif
                                            @endif
                                             <a class="btn btn-sm btn-danger button1" href="{{ url('arrival/print/'. $a->id) }}">
                                                <i class="fa fa-file"></i>
                                            </a>
                                          <!--   <a class="btn btn-sm btn-warning button1" href="{{ url('arrival/verify/'. $a->id) }}">
                                                <i class="fa fa-times"></i>
                                            </a> -->
                                            @if($a->status == "unverified")
                                            <a class="btn btn-sm btn-danger button1" href="{{ url('arrival/delete/' . $a->id) }}" onclick="return confirm('tenan ki meh dihapus?')">
                                            <i class="fa fa-trash"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection