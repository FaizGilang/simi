@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-md-12">

            <div class="col-md-4" style="padding:0; margin-top:-30px; ">
                <a class="btn btn-md btn-white" href="{{ url()->current() }}" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px outline: 0; box-shadow: none;">
                    <h4 class="">Detail Penerimaan Barang</h4>
                </a>    
            </div>
                 
            </div>
            </div>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
            <tr>
                <td>Tahun Anggaran</td>
                <td>{{$data->year}}</td>
            </tr>

            <tr>
                <td>Status</td>
                @if($data->status === "verified")
                <td>Diterima</td>
                @elseif($data->status === "unverified")
                <td>Menunggu Verifikasi</td>
                @else
                <td>Ditolah</td>
                @endif
            </tr>

        </table>

        <h3>Barang</h3>
        <table class="table table-hover table-bordered">
            <tr>
                <th>No.</th>
                <th>Nama Barang</th>
                <th>Sumber</th>
                <th>Jumlah</th>
            </tr>
           @foreach($data1 as $index=>$a)
            <tr>
                <th>{{$index+1}}</th>
                <th>{{$a->name}}</th>
                @if($a->fund_source === "1")
                <th>APBN</th>
                @elseif($a->fund_source === "2")
                <th>APBD</th>
                @else
                <th>Lainya</th>
                @endif
                <th>{{$a->amount}}</th>
            </tr>
            @endforeach 
              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
