<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
</head>
<style>

.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 5px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}

img{
    height: 110px; 
    width: 110px; 
    padding: 0px;
    margin-left: 0px; 
    margin-bottom: 0px;
    margin-top: -20px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
p{
    margin: 0px;
    padding: 0px;
}

.column {
  padding: 0px;
  height: 0px; /* Should be removed. Only for demonstration */
}

.left, .right {
  width: 25%;
}

.middle {
  width: 80%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>

<div class="row">
    <div class="column left">
        <img style="float: left;" src="admin/img/AKN.jpg" >
    </div>
    <div class="column middle" style="text-align: center;" > 
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 12px;" align="center">KEMENTERIAN RISET TEKNOLOGI DAN PENDIDIKAN TINGGI</p>   <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 12px;" align="center"> POLITEKNIK NEGERI JAKARTA</p> <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 12px;" align="center"> PROGRAM STUDI DILUAR DOMISILI ( PDD )</p> <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 14px;" align="center">AKADEMI KOMUNITAS NEGERI DEMAK</p> <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 10px;" align="center">Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</p>
    </div>
    <div class=" columnright">
        <img class="img" style="float: right;" src="admin/img/PNJ.jpg">
    </div>
</div>
<hr style="margin-top: 0px;">
<br>
<p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 14px" align="center">
BERITA ACARA SERAH TERIMA BARANG INVENTARIS</p>

<br>
<br>
<p style=" line-height: 1px; font-family: Times New Roman; font-size: 12px">Pada tanggal {{ Carbon\Carbon::now()->format('d')}} bulan {{ Carbon\Carbon::now()->format('m')}} tahun {{ Carbon\Carbon::now()->format('Y')}} telah terjadi serah terima barang 
inventaris antara : </p>
<br>
<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px"> 1. {{$from->name}}</p> <br>
<p style="margin-left: 63px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Sebagai pihak yang menyerahkan</p> <br><br>

<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px"> 2. {{$to->name}}</p> <br>
<p style="margin-left: 63px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Sebagai pihak yang menerima</p> <br><br>

<!-- /.row -->
<p style="padding-bottom: 5px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Barang yang diserahkan :</p>
<div class="col-md-4">

 <table class="center" align="center" >
    <thead>
       <tr>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">No.</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Nama Barang</p></th>
        <!--  <th width="2px" style="text-align:center;">Merk</th> -->
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Jumlah</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Keadaan Barang</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Lokasi Barang</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Ket</p></th>
    </tr>
</thead>
<tbody>
  <?php $count = 1; 
  $jumlahsks = 0;
  ?> 
  @foreach($item as $index=>$a)
  <tr> 
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">{{$index+1}}.</p></td> 
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">{{$a->name}}</p></td>
    <!-- <td style="text-align:left;"></td>   -->
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">{{$a->amount}}</p></td>  
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">B</p></td>  
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">-</p></td> 
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">-</p></td> 

</tr> 
@endforeach

</tbody>
</table>  
<p style="margin-top: 20px; padding-bottom: -15px; line-height: 1px; font-weight:bold;  font-family: Times New Roman; font-size: 12px">Keterangan : </p><br>
<p style="padding-bottom: -15px; font-family: Times New Roman; font-size: 12px">1.Pengadaan barang-barang tersebut untuk mendukung kegiatan pekerjaan dibagian saudara
bekerja.</p><br>                                   
<p style="padding-bottom: -15px; font-family: Times New Roman; font-size: 12px">2.Bersedia mengembalikan ke AKN apabila saudara tidak bekerja diruang/bagian tersebut.</p><br>
<p style="padding-bottom: -15px; font-family: Times New Roman; font-size: 12px">3.Bertangung jawab penuh terhadap keamanan barang tersebut.</p> <br>
<p style="padding-bottom: -15px; font-family: Times New Roman; font-size: 12px">4.Bersedia mengganti barang tersebut apabila hilang,dikarenakan kelalaian.</p><br>
<p style="font-family: Times New Roman; font-size: 12px">Sesuai undang-undang nomor 17 tahun 2003 pasal 35 ayat (1) TGR (tentang ganti rugi) Barang Milik Negara</p><br><br>



<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Menyerahkan</p> <p style="margin-left: 550px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Menerima</p> <br>

<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Pihak Pertama</p>  <p style="margin-left: 550px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Pihak Kedua</p><br><br><br> <br>
<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px">{{$from->name}}</p>  <p style="margin-left: 550px; line-height: 1px; font-family: Times New Roman; font-size: 12px">{{$to->name}}</p><br>

<p style="margin-left: 300px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Mengetahui</p> <br>
<p style="margin-left: 300px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Kasubbag KAU</p> <br><br><br> <br> 
<p style="margin-left: 300px; line-height: 1px; font-family: Times New Roman; font-size: 12px">{{$user->name}}</p>  

</div>
</html>