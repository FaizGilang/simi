@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
           <h4 class="card-title">Serah Terima Barang</h4>
           <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}

            <div class="form-group label-floating">
                     <select class="form-control" name="from">
                         <option value="">Pihak Yang Menyerahkan</option>
                         @foreach($data as $b)
                         <option value="{{ $b->id }}">{{ $b->name }}</option>
                         @endforeach
                     </select>
            </div>
            <div class="form-group label-floating">
                     <select class="form-control" name="to">
                         <option value="">Pihak Yang Menerima</option>
                         @foreach($data as $b)
                         <option value="{{ $b->id }}">{{ $b->name }}</option>
                         @endforeach
                     </select>
            </div>
          <div class="form-group label-floating col-md-12" id="form"></div>
        <button type="submit" class="btn btn-fill btn-info">Submit</button>
    </form>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  jQuery(document).ready(function()
  {
      jQuery('select[name="from"]').on('change',function()
      {
          var id = jQuery(this).val();
          var A = jQuery(this).val();
          var id = "/"+id;
            $('#button1').remove();
                $('#button2').remove();
                $('#button3').remove();

               
                  jQuery.ajax({
                     url : '/distribution/data/' +A,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
               
                        jQuery.each(data, function(key,value){
                              $('#form').append('<div class="form-group col-md-2"><input style="text-align:center; margin-left: 35px;" type="checkbox" name="field_id[]" value='+value.id+'></div><div class="form-group col-md-5"><input  class="form-control" type="text" placeholder="" name="field_item[]" value='+value.name+' disabled></div><div class="form-group col-md-5  "><input  class="form-control" type="text" placeholder="" name="field_number[]" value='+value.number+' disabled></div>');
                        });
                     }
                  });    


            }); 
                  
          
      });
    
  

</script>

@endsection
