@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
           <h4 class="card-title">Serah Terima Barang</h4>
           <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}

            <div class="form-group label-floating">
                     <select class="form-control" name="to">
                         <option value="{{$data->iduser}}">{{$data->name}}</option>
                         @foreach($data2 as $b)
                         <option value="{{ $b->id }}">{{ $b->name }}</option>
                         @endforeach
                     </select>
            </div>
             <div class=" field_wrapper">
                <label>Penerimaan Barang <a href="javascript:void(0);" class="add_button btn btn-info btn-xs" title="Add field">Add items</a></label>
                <br/>
    
                <div class="col-xs-12">
                @foreach($data1 as $index=>$a)
                <div class="form-group col-xs-5">
                     <select class="form-control" name="field_name[]">
                         <option value="{{$a->item_id}}">{{$a->name}}</option>
                         @foreach($item as $b)
                             <option value="{{ $b->id }}">{{ $b->name }}</option>
                         @endforeach
                     </select>
                 </div>
                <div class="form-group col-xs-5">
                    <input  class="form-control" type="number" placeholder="Jumlah" name="field_amount[]" value="{{$a->amount}}"/>
                </div>
                <div class="col-xs-2">
                <a href="{{ url('distribution/delete1/' . $a->iddetail) }}" class="btn btn-danger btn-sm" style="margin-top:25px;" title="Remove field"><i class="fa fa-trash"></i></a>
                </div>
                @endforeach
                </div>
        </div>
        <button type="submit" class="btn btn-fill btn-info">Submit</button>
    </form>
</div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
  var maxField = 999; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<div class="col-xs-12"><div class="form-group col-xs-5"><select class="form-control" name="field_name[]"><option value="">Barang</option>@foreach($item as $a)<option value="{{ $a->id }}">{{ $a->name }}</option>@endforeach</select></div><div class="form-group col-xs-5"><input  class="form-control" type="number" placeholder="Jumlah" name="field_amount[]" value=""/></div>&nbsp;<a href="javascript:void(0);" class="remove_button btn btn-danger btn-sm" style="margin-top:25px;" title="Remove field"><i class="fa fa-trash"></i></a></div>'; //New input field html
  var x = 1; //Initial field counter is 1

  $(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); // Add field html
    }
  });
  $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
  });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\AddDistributionRequest', '#my-form'); !!}
@endsection
