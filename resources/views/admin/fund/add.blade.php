@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Pilih Jenis Pendanaan</h4>
            <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
                <div class="form-group label-floating">
                    <label class="control-label">Tahun Anggaran</label>
                    <input type="text" name="year" class="form-control ">
                </div>
             <div class="form-group field_wrapper">
                <label>Sumber Dana <a href="javascript:void(0);" class="add_button btn btn-info btn-xs" title="Add field">Add items</a></label>
                <br/>
                <div>
                    <input placeholder="Sumber" type="text" name="field_name[]" value=""/>
                    <input type="number" placeholder="Jumlah" name="field_price[]" value=""/>
                </div>
            </div>
                <button type="submit" name="save" value="save" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection

