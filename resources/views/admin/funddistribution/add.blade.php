@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Pilih Jenis Pendanaan</h4>
            <form method="post" action="{{ url()->current() }} " enctype="multipart/form-data">
            {{ csrf_field() }}
                 
                <div>
                    <select class="form-control label-floating" name="year">
                        <option value="">Tahun Anggaran</option>
                        @foreach($year as $a)
                        <option value="{{$a->year}}">{{$a->year}}</option>
                        @endforeach
                    </select>
                </div>

                <div>
                    <select class="form-control label-floating" name="division">
                        <option value="">Divisi</option>
        
                    </select>
                </div>
             
                <div class="form-group label-floating">
                    <label class="control-label">Jumlah</label>
                    <input type="number" name="amount" class="form-control ">
                </div>

                <button type="submit" name="save" value="save" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="year"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/funddis/data/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="division"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="division"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="division"]').empty();
            }
        });
    });
</script>
@endsection

