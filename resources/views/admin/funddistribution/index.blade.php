@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-xs-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-xs-12">

            <div class="col-xs-4" style="padding:0; margin-top:-20px; ">
                <a class="btn btn-lg btn-white" href="/funddis" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 0; box-shadow: none;">
                    <h4 class="">Pendanaan</h4>
                </a>
                 
            </div>
            <div class="col-xs-4" style="padding:0; margin-top: -10px;">
                <a class="btn btn-md" href="/fund" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 1px; box-shadow: none;">
                     <h4 class="">Sumber Pendanaan</h4>
                </a>
            </div>
            </div>
            </div>
                <div class="card-content">
                      
                    <div class="table-responsive">
                        <table class="table table-striped table1 table-bordered">
                            <thead class="text-info">
                                <tr>
                                    <th>No</th>
                                    <th>Divisi</th>
                                    <th>Jumlah</th>
                                    <th>Terpakai</th>
                                    <th>Tahun Anggaran</th>
                                    <th>Action  <a class="btn btn-info btn-sm" style="margin-left:10px;  margin-top: -5px; margin-bottom: -5px;" href="{{ url('funddis/add')}}">
                                            <i class="fa fa-plus"></i>
                                        </a></th>
                                </thead>

                                <tbody>
                                    @foreach($data as $index=>$a)
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{ $a->name }}</td>
                                        <td>{{ number_format($a->amount) }}</td>
                                        <td>{{ number_format($a->used) }}</td>
                                        <td>{{ $a->year}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-success button1" href="{{ url('user/' . $a->id . '/edit') }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a class="btn btn-sm btn-danger button1" href="{{ url('user/' . $a->id . '/delete') }}" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
 