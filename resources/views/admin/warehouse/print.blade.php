<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
</head>
<style>

.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 5px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}

img{
    height: 110px; 
    width: 110px; 
    padding: 0px;
    margin-left: 0px; 
    margin-bottom: 0px;
    margin-top: -20px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
p{
    margin: 0px;
    padding: 0px;
}

.column {
  padding: 0px;
  height: 0px; /* Should be removed. Only for demonstration */
}

.left, .right {
  width: 25%;
}

.middle {
  width: 80%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>

<div class="row">
    <div class="column left">
        <img style="float: left;" src="admin/img/AKN.jpg" >
    </div>
    <div class="column middle" style="text-align: center;" > 
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 12px;" align="center">KEMENTERIAN RISET TEKNOLOGI DAN PENDIDIKAN TINGGI</p>   <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 12px;" align="center"> POLITEKNIK NEGERI JAKARTA</p> <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 12px;" align="center"> PROGRAM STUDI DILUAR DOMISILI ( PDD )</p> <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 14px;" align="center">AKADEMI KOMUNITAS NEGERI DEMAK</p> <br>
        <p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 10px;" align="center">Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</p>
    </div>
    <div class=" columnright">
        <img class="img" style="float: right;" src="admin/img/PNJ.jpg">
    </div>
</div>
<hr style="margin-top: 0px;">
<br>
<p style=" line-height: 1px; font-family: Times New Roman; font-weight: bold; font-size: 14px" align="center">
BERITA ACARA PENGHAPUSAN BARANG INVENTARIS</p>

<br>
<br>
<p style=" line-height: 1px; font-family: Times New Roman; font-size: 14px">Pada tanggal {{ Carbon\Carbon::now()->format('d')}} bulan {{ Carbon\Carbon::now()->format('m')}} tahun {{ Carbon\Carbon::now()->format('Y')}} telah dilaksanakan penghapusan barang berupa: </p><br><br>

<!-- /.row -->
<div class="col-md-4">

 <table class="center" align="center" >
    <thead>
       <tr>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">No.</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Nama Barang</p></th>
        <!--  <th width="2px" style="text-align:center;">Merk</th> -->
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Nomer</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Lokasi Barang</p></th>
        <th width="2px" style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">Ket</p></th>
    </tr>
</thead>
<tbody>
  <?php $count = 1; 
  $jumlahsks = 0;
  ?> 
  @foreach($item as $index=>$a)
  <tr> 
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">{{$index+1}}.</p></td> 
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">{{$a->name}}</p></td>
    <!-- <td style="text-align:left;"></td>   -->
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">{{$a->number}}</p></td>  
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">-</p></td> 
    <td style="text-align:center;"><p style=" font-family: Times New Roman; font-size: 12px">-</p></td> 

</tr> 
@endforeach

</tbody>
</table>  
<br><br>
<p style=" text-align: justify; padding-bottom: 5px; line-height: 1px; font-family: Times New Roman; font-size: 14px">Barang tersebut telah diperiksa dan dinyatakan tidak dapat lagi digunakan</p><br>
<p style=" text-align: justify; padding-bottom: 5px; line-height: 1px; font-family: Times New Roman; font-size: 14px">Demikian surat ini kamu buat berdasarkan keadaan sebenarnya. Atas perhatian dan kerja samanya kami ucapkan terima kasih</p><br><br>


<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Mengajukan</p> <p style="margin-left: 550px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Menyetujui</p> <br>

<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Subbag KAU</p>  <p style="margin-left: 550px; line-height: 1px; font-family: Times New Roman; font-size: 12px">Kasubbag KAU</p><br><br><br> <br>
<p style="margin-left: 50px; line-height: 1px; font-family: Times New Roman; font-size: 12px">{{$user->name}}</p>  <p style="margin-left: 550px; line-height: 1px; font-family: Times New Roman; font-size: 12px">{{$user1->name}}</p>


</div>
</html>