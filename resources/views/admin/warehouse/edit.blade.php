@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Ubah Data Barang</h4>
            <form method="post" action="{{ url()->current() }} " enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group label-floating">
                    <label class="control-label">Nama Barang</label>
                    <input type="text" name="name" disabled class="form-control" value="{{$data->name}}">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Vendor</label>
                    <input type="text" name="vendor" class="form-control " disabled value="{{$data->vendor}}">
                </div>

                 <div class="form-group label-floating">
                    <label class="control-label">Nomor</label>
                    <input type="text" name="number" class="form-control " disabled value="{{$data->number}}">
                </div>

                 <div class="form-group label-floating">
                    <label class="control-label">Keaddan Barang</label>
                    <input type="text" name="condition" class="form-control " disabled value="{{$data->condition}}">
                </div>

                 <div class="form-group label-floating">
                    <label class="control-label">Letak Barang</label>
                    <input type="text" name="location" class="form-control "  value="{{$data->location}}">
                </div>

                <button type="submit" name="save" value="save" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection

