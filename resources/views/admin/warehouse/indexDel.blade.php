@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-xs-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-xs-12">
 <div class="col-xs-4" style="padding:0; margin-top: -10px;">
                <a class="btn btn-md" href="/warehouse/" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 1px; box-shadow: none;">
                     <h4 class="">Belum Terdistribusi</h4>
                </a>
            </div>
			
			  <div class="col-xs-4" style="padding:0; margin-top: -10px;">
                <a class="btn btn-md" href="/warehouse/all" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 1px; box-shadow: none;">
                     <h4 class="">Semua Barang</h4>
                </a>
            </div>
            <div class="col-xs-4" style="padding:0; margin-top:-20px; ">
                <a class="btn btn-lg btn-white" href="/warehouse/del" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px; outline: 0; box-shadow: none;">
                    <h4 class="">Barang Dihapus</h4>
                </a>
                 
            </div>
			 
          
            </div>
            </div>
                <div class="card-content">
                      
                    <div class="material-datatables">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
         <thead class="text-info">
                   <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Nomor</th>
                                    <th>Digunakan Oleh</th>
                                    <th>Kondisi</th>
                                    <th>Status</th>
                                    <th>Ruangan</th>
         
                                </thead>
                                <tbody>
                                    <?php $count = 1; ?> 
            @if(Auth()->user()->occupation === 7 || Auth()->user()->occupation === 10)
                                	@foreach($data1 as $index=>$a)
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $a->name }}</td>
                                        <td>{{ $a->number }}</td>
                                        <td>-</td>
                                        <td>{{ $a->condition }}</td>
                                        @if($a->status === "active")
                                        <td>Aktif</td>
                                        @elseif($a->status === "delete")
                                        <td>Dalam Penanganan</td>
                                        @else
                                        <td>Dihapuskan</td>
                                        @endif
                                        <td>{{ $a->location }}</td>
                                       
                                      
                                    </tr>
                                     <?php $count++; ?> 
                                    @endforeach
                                @endif
                                    @foreach($data as $index=>$a)
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $a->name }}</td>
                                        <td>{{ $a->number }}</td>
                                        <td>{{ $a->divname }}</td>
                                        <td>{{ $a->condition }}</td>
                                        @if($a->status === "active")
                                        <td>Aktif</td>
                                        @elseif($a->status === "delete")
                                        <td>Dalam Penanganan</td>
                                        @else
                                        <td>Dihapuskan</td>
                                        @endif
                                        <td>{{ $a->Location }}</td>
                                        <td>
                                           <a class="btn btn-sm btn-success button1" href="{{ url('warehouse/edit/'. $a->id) }}">
                                                                                          <i class="fa fa-pencil"></i>
                                                                                      </a>
                                            @if($a->status != "delete")
                                           <a class="btn btn-sm btn-danger button1" href="{{ url('warehouse/delete/'. $a->id) }}">
                                                <i class="fa fa-eraser"></i>
                                            </a>
                                            @endif
                                            
                                            @if(Auth()->user()->occupation === 7 || Auth()->user()->occupation === 10 && $a->status == "delete")
                                            <a class="btn btn-sm btn-info button1" href="{{ url('warehouse/delete1/'. $a->id) }}">
                                                <i class="fa fa-wrench"></i>
                                            </a> 
                                            @endif
                                            <!-- <a class="btn btn-sm btn-danger button1" href="{{ url('warehouse/delete/'. $a->id) }}">
                                                <i class="fa fa-dropbox"></i>
                                            </a> -->
                                        </td>
                                    </tr>
                                     <?php $count++; ?> 
                                    @endforeach
                                </tbody>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection