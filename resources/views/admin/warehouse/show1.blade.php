@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-md-12">

            <div class="col-md-4" style="padding:0; margin-top:-30px; ">
                <a class="btn btn-md btn-white" href="{{ url()->current() }}" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px outline: 0; box-shadow: none;">
                    <h4 class="">Detail Barang Inventaris</h4>
                </a>    
            </div>

            </div>
            </div>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
           
            <tr>
                <td>Nama Barang</td>
                <td>{{$data1->name}}</td>
            </tr>

            <tr>
                <td>Nomer Barang </td>
                <td>{{$data1->number}}</td>
            </tr>
            <tr>
                <td>Sumber Dana </td>
                 @if($data1->fund_source === "1")
                <td>APBN</td>
                @elseif($data1->fund_source === "2")
                <td>APBD</td>
                @else
                <td>Lainya</td>
                @endif
            </tr>
            <tr>
                <td>Tahun Pengadaan</td>
                <td>{{$data1->year}}</td>
            </tr>
            <tr>
                <td>Vendor </td>
                <td>{{$data1->vendor}}</td>
            </tr>
            <tr>
                <td>Pengguna </td>
                <td>Belum Dibagikan</td>
            </tr>
            <tr>
                <td>Kondisi Barang </td>
                <td>{{$data1->condition}}</td>
            </tr>
            <tr>
                <td>Lokasi Barang</td>
                <td>{{$data1->location}}</td>
            </tr>
            <tr>
                <td>Berita Acara Penerimaan</td>
                <td><a class="btn btn-sm btn-primary button1" href="{{ url('arrival/detail/'. $data1->arrival_doc) }}">
                                               <i class="fa fa-info"></i>
                                            </a></td>
            </tr>
            <tr>
                <td>Berita Acara Serah Terima </td>
                <td><a class="btn btn-sm btn-primary button1" href="{{ url('distribution/detail/'. $data1->distribute_doc) }}">
                                               <i class="fa fa-info"></i>
                                            </a></td>
            </tr>
        </table>

      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
