@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Alasan Laporan</h4>
            <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}
                <div>
                <select class="form-control label-floating" name="description">
                         <option value="">Alasan</option>
                         <option value="Rusak">Rusak</option>
                         <option value="Dicuri">Dicuri</option>
                         <option value="Hilang">Hilang</option>
                         <option value="Lainya">Lainnya</option>
                </select>
                </div>
                <button type="submit" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
