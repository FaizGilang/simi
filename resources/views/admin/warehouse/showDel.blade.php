@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-md-12">

            <div class="col-md-4" style="padding:0; margin-top:-30px; ">
                <a class="btn btn-md btn-white" href="{{ url()->current() }}" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px outline: 0; box-shadow: none;">
                    <h4 class="">Detail Pengapusan Barang</h4>
                </a>    
            </div>
            <?php $count = 0; ?> 
            @foreach($data1 as $index=>$a)
                <?php $count =$count + 1; ?> 
            @endforeach 

            </div>
            </div>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
           
            <tr>
                <td>Tahun Pengapusan</td>
                <td>{{$data}}</td>
            </tr>

            <tr>
                <td>Jumlah </td>
                <td>{{$count}}</td>
            </tr>

        </table>

        <h3>Daftar Barang</h3>
        <table class="table table-hover table-bordered">
            <tr>
                <th>No.</th>
                <th>Nomer</th>
                <th>Sumber</th>
                <th>Tahun</th>
            </tr>
           @foreach($data1 as $index=>$a)
            <tr>
                <th>{{$index+1}}</th>
                <th>{{$a->number}}</th>
                @if($a->fund_source === "1")
                <th>APBN</th>
                @elseif($a->fund_source === "2")
                <th>APBD</th>
                @else
                <th>Lainya</th>
                @endif
                <th>{{$a->year}}</th>
            </tr>
            @endforeach 
              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
