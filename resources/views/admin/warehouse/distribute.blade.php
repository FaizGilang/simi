@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
           <h4 class="card-title">Procurement</h4>
           <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}

            <div class="form-group label-floating">
                    <label class="control-label">Tahun Pengadaan</label>
                    <input type="text" name="year" class="form-control">
            </div>
            <div class=" field_wrapper">
                <label>Permohonan Barang <a href="javascript:void(0);" class="add_button btn btn-info btn-xs" title="Add field">Add items</a></label>
                <br/>
    
                <div class="col-xs-12">

                <div class="form-group col-xs-4">
                     <select class="form-control" name="field_name[]">
                         <option value="">Barang</option>
                         @foreach($item as $a)
                         <option value="{{ $a->id }}">{{ $a->name }}</option>
                         @endforeach
                     </select>
                 </div>
                  <div class="form-group col-xs-4">
                     <select class="form-control" name="field_source[]">
                         <option value="">Sumber Dana</option>
                         <option value="1">APBN</option>
                         <option value="2">APBD</option>
                         <option value="3">Lainya</option>
                     </select>
                 </div>
                <div class="form-group col-xs-2">
                    <input  class="form-control" type="number" placeholder="Jumlah" name="field_amount[]" value=""/>
                </div>
                </div>
        </div>
        <button type="submit" class="btn btn-fill btn-info">Submit</button>
    </form>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
  var maxField = 999; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<div class="col-xs-12"><div class="form-group col-xs-4"><select class="form-control" name="field_name[]"><option value="">Barang</option>@foreach($item as $a)<option value="{{ $a->id }}">{{ $a->name }}</option>@endforeach</select></div>&nbsp;<div class="form-group col-xs-4"><select class="form-control" name="field_source[]"><option value="">Sumber Dana</option><option value="1">APBN</option><option value="2">APBD</option><option value="3">Lainya</option></select></div>&nbsp;<div class="form-group col-xs-2"><input  class="form-control" type="number" placeholder="Jumlah" name="field_amount[]" value=""/></div>&nbsp;<a href="javascript:void(0);" class="remove_button btn btn-danger btn-sm" style="margin-top:25px;" title="Remove field"><i class="fa fa-trash"></i></a></div>'; //New input field html
  var x = 1; //Initial field counter is 1

  $(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); // Add field html
    }
  });
  $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
  });
});
</script>
@endsection
