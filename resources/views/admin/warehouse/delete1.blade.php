@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Jenis Penanganan</h4>
            <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}
                <div>
                <select class="form-control label-floating" name="description">
                         <option value="">Jenis</option>
                         <option value="1">Perbaikan</option>
                         <option value="2">Penghapusan</option>
                </select>
                </div>
				
				 <div>
                <select class="form-control label-floating" name="condition">
                         <option value="">Keadaan</option>
                         <option value="Baik">Baik</option>
                         <option value="Kurang Baik">Kurang Baik</option>
                         <option value="Buruk">Buruk</option>
                </select>
                </div>
                <button type="submit" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
