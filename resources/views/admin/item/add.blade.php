@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Tambah Data Barang</h4>
            <form method="post" action="{{ url()->current() }} " enctype="multipart/form-data" id="my-form">
            {{ csrf_field() }}
                <div class="form-group label-floating">
                    <label class="control-label">Nama Barang</label>
                    <input type="text" name="name" class="form-control ">
                </div>
                <div class="form-group label-floating">
                    <select class="form-control" name="category">
                        <option value="">Pilih Kategori Barang</option>
                        @foreach($category as $a)
                        <option value="{{$a->id}}">{{$a->name}}</option>
                        @endforeach
                    </select>
                </div>
				 <div class="form-group label-floating">
                    <label class="control-label">Spesifikasi</label>
                    <input type="textarea" name="specification" class="form-control ">
                </div>
                 <div class="form-group label-floating">
                    <label class="control-label">Kode Barang</label>
                    <input type="text" name="code" class="form-control ">
                </div>

           
                <button type="submit" name="save" value="save" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\AddItemRequest', '#my-form'); !!}
@endsection