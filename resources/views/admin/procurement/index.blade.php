@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-md-12">

            <div class="col-md-4" style="padding:0; margin-top:-30px; ">
                <a class="btn btn-md btn-white" href="/procurement" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px outline: 0; box-shadow: none;">
                    <h4 class="">Procurement</h4>
                </a>    
            </div>
                 
            </div>
            </div>
                <div class="card-content">
                    <div class="table-responsive">
                         <table class="table table-striped table-bordered" >
                            <thead class="text-info">
                                <tr>
                                    <th>No.</th>
                                    <th>Pemohon</th>
                                    <th>Divisi</th>
                                    <th>Tahap</th>
                                    <th>Keterangan</th>
                                    <th class="text-center">Action   <a data-toggle="tooltip" title="Hooray!" class="btn btn-info btn-sm red-tooltip" href="{{ url('procurement/add')}}">
                                            <i class="fa fa-plus"></i>
                                        </a></th>
                                      
                                </thead>
                                <tbody>
                                    @foreach($data as $index=>$a)
                                    <tr>
                                        <td>{{ $index+1 }}.</td>
                                        <td>{{ $a->name }}</td>
                                        <td>{{ $a->division }}</td>
                                        @if($a->status ==='accept1' )
                                            <td>
                                                <span class="fa fa-check-square-o"></span> Verifikasi KAU<br>
                                                <span class="fa fa-square-o"></span> Kasubag KAU<br>
                                                <span class="fa fa-square-o"></span> Kabag {{$a->division}}
                                            </td>
                                        @elseif($a->status === 'accept2')
                                        <td>
                                                <span class="fa fa-check-square-o"></span> Verifikasi KAU<br>
                                                <span class="fa fa-check-square-o"></span> Kasubag KAU<br>
                                                <span class="fa fa-square-o"></span> Kabag {{$a->division}}
                                            </td>  
                                      @elseif($a->status === 'accept3')
                                        <td>
                                                <span class="fa fa-check-square-o"></span> Verifikasi KAU<br>
                                                <span class="fa fa-check-square-o"></span> Kasubag KAU<br>
                                                <span class="fa fa-check-square-o"></span> Kabag {{$a->division}}
                                            </td>
                                        @else
                                              <td>
                                                <span class="fa fa-square-o"></span> Verifikasi KAU<br>
                                                <span class="fa fa-square-o"></span> Kasubag KAU<br>
                                                <span class="fa fa-square-o"></span> Kabag {{$a->division}}
                                            </td>
                                       @endif 

                                        @if($a->status ==='accept1')
                                            <td>Menunggu Persetujuan Kassubag KAU</td>
                                        @elseif($a->status === 'accept2')
                                           <td>Menunggu Persetujuan Koordinator.</td>
                                       @elseif($a->status === 'accept3')
                                           <td>Menunggu Penganggaran.</td>
                                        @elseif($a->status === 'applied')
                                           <td>Menunggu Persetujuan Kadiv {{$a->division}}</td>
                                        @else
                                             <td > <span class="label label-default label-block label1"><b>Waiting</b></span></td>
                                       @endif    
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-primary button1" href="{{ url('occupation/' . $a->id . '/edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                             <a class="btn btn-sm btn-info button1" href="{{ url('procurement/' . $a->id . '/detail') }}">
                                                <i class="fa fa-list"></i>
                                            </a>                                       @if($a->status ==='accept3')
                                            <a class="btn btn-sm btn-success button1" href="{{ url('procurement/' . $a->id . '/fund') }}">
                                                <i class="fa fa-dollar"></i>
                                            </a>
                                            @else
                                            <a class="btn btn-sm btn-success button1" href="{{ url('procurement/' . $a->id . '/accept') }}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                            @endif
                                            <a class="btn btn-sm btn-danger button1" href="{{ url('occupation/' . $a->id . '/delete') }}" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
