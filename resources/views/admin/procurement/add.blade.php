@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
           <h4 class="card-title">Procurement</h4>
             @foreach ($item as $product)
                    <div class="col-md-3">
                        <div class="thumbnail" style="background-color: #f5f5f5;">
                            <div class="caption text-center">
                                <a href="#" class="edit-modal" style="border: 0px; margin:0px;padding: 0px;" data-item="{{$product->id}}" data-id="{{$product->name}}" data-title="{{$product->vendor}}" data-content=""><img src="{{ asset('items/'.$product->image) }}" alt="product" class="img-responsive"></a>
                                <a class="text-info"><h3>{{ $product->name }}</h3>
                                <p class="text-info">Rp.{{ number_format($product->price) }}</p>
                                </a>
                            </div> <!-- end caption -->
                        </div> <!-- end thumbnail -->
                    </div> <!-- end col-md-3 -->
                @endforeach
        </div>
    </div>
    <div class="card">
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
           <h4 class="card-title">Procurement</h4>
           <table class="table">
                    <tr>
                        <th>No.</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                    @foreach (Cart::content() as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->qty }}</td>
                        <td>Rp.{{ number_format($item->price) }}</td>
                    </tr>

                    @endforeach
            </table>
                    <a href="/procurement/save" class="btn btn-info pull-right">Simpan</a>
        </div>
    </div>
    
    </div>
</div>

<div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                                                     <input type="hidden" class="form-control" id="item_edit" disabled>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">Nama:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="id_edit" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Vendor:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="title_edit" disabled>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="content">Jumlah:</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="content_edit" >
                                <p class="errorContent text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Tambah
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection
@section('script')
     <script>
        $(window).load(function(){
            $('#postTable').removeAttr('style');
        })
    </script>

    <script>
        $(document).ready(function(){
            $('.published').iCheck({
                checkboxClass: 'icheckbox_square-yellow',
                radioClass: 'iradio_square-yellow',
                increaseArea: '20%'
            });
            $('.published').on('ifClicked', function(event){
                id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::route('changeStatus') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
            $('.published').on('ifToggled', function(event) {
                $(this).closest('tr').toggleClass('warning');
            });
        });
        
    </script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        
        // delete a post
        $(document).on('click', '.edit-modal', function() {
            $('.modal-title').text('Masukan ke Permohonan');
            $('#item_edit').val($(this).data('item'));
            $('#id_edit').val($(this).data('id'));
            $('#title_edit').val($(this).data('title'));
            $('#content_edit').val($(this).data('content'));
            id = $('#item_edit').val();
            $('#editModal').modal('show');
        });
        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'GET',
                url: '/procurement/additem/'+id+'/'+$('#content_edit').val(),
                success: function(data) {
                  window.location.href = '{{ url('/procurement/add') }}';
                 }
            });
        });
    </script>

@endsection