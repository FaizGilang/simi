@extends('layouts.admin')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
         <div class="card">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <div class="row">
            <div class="col-md-12">
                 
            </div>
            </div>
            <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-file fa-2x"></i>
        </div>
                <div class="card-content">
            <h4 class="card-title">Procurement Detail</h4>  
                    <div class="table-responsive">
                        <table class="table table-hover">
            <tr>
                <td>Pemohon</td>
                <td>{{$data->name}}</td>
            </tr>

            <tr>
                <td>Jabatan</td>
                <td>{{$data->occuname}}</td>
            </tr>
            <tr>
                <td>Divisi</td>
                <td>{{$data->divname}}</td>
            </tr>

            <tr>
                <td>Status</td>
                <td>{{$data->status}}</td>
            </tr>

            <tr>
                <td>Tanggal Diajukan</td>
                <td>{{$data->created_at}}</td>
            </tr>
        </table>

        <h3>Quotation Items</h3>
        <table class="table table-hover table-bordered">
            <tr>
                <th>No.</th>
                <th>Nama Barang</th>
                <th>Satuan</th>
                <th>Jumlah</th>
                <th>Harga Satuan</th>
                <th>Harga +PPH</th>
                <th>Harga Total</th>
                <th>Deskripsi</th>
            </tr>
            @foreach($procurement as $index=>$a)
            <tr>
                <th>{{$index+1}}</th>
                <th>{{$a->name}}</th>
                <th>{{$a->unit}}</th>
                <th>{{$a->amount}}</th>
                <th>{{number_format($a->price)}}</th>
                <th>{{number_format($a->price+(30/100*$a->price))}}</th>
                <th>RP. {{number_format(($a->price+(30/100*$a->price))*$a->amount)}}</th>
                <th>{{$a->desc}}</th>
            </tr>
            @endforeach
              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
