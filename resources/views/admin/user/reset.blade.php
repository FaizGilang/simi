@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="card">

                 <div class="card-content">
           <h4 class="card-title">Permohonan Reset Password</h4>
            <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}
                <div class="form-group label-floating">
                    <label class="control-label">Masukan Email</label>
                    <input type="email" name="email" class="form-control">
                </div>
              
                <button type="submit" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
            </div>
        </div>
    </div>
</div>
@endsection
