@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card-header card-header-icon" data-background-color="blue">
            <i class="fa fa-user fa-2x"></i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Ubah Data Jabatan</h4>
            <form method="post" action="{{ url()->current() }}" id="my-form">
            {{ csrf_field() }}
                <div class="form-group label-floating">
                    <label class="control-label">Jabatan</label>
                <input type="text" name="name" class="form-control" value="{{$data->name}}">
                </div>
                <select class="form-control label-floating" name="occupation">
                    <option value="{{$data->division_id}}">{{$data->divname}}</option>
                    @foreach($data1 as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
       
                <button type="submit" class="btn btn-fill btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\EditOccupationRequest', '#my-form'); !!}
@endsection