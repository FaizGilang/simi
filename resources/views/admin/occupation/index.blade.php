@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
     <div class="card">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-4" style="padding:0; margin-top:-10px; ">
                    <a class="btn btn-md" href="/user" style="width:100%; margin-left:0; margin-right:0;margin-bottom: -10px; outline: 0; box-shadow: none;">
                        <h4 class="">Pengguna</h4>
                    </a>    
                </div>
                <div class="col-md-4" style="padding:0; margin-top: -20px;">
                    <a class="btn btn-lg btn-white" href="/occupation" style="width:100%; margin-left:0; margin-right:0; margin-bottom: -10px;  outline: 1px; box-shadow: none;">
                     <h4 class="">Jabatan</h4>
                 </a>
             </div>
             <div class="col-md-4" style="padding:0; margin-top: -10px;">
                <a class="btn btn-md" href="/division" style="width:100%; margin-left:0; margin-right:0;  margin-bottom: -10px; outline: 0; box-shadow: none;">
                    <h4 class="">Divisi</h4>
                </a>
            </div>

        </div>
    </div>
    <div class="card-content">
      <div class="material-datatables">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
         <thead class="text-info">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Divisi</th>
                <th>Aksi <a class="btn btn-info btn-sm" style="margin-left:10px;  margin-top: -5px; margin-bottom: -5px;" href="{{ url('occupation/add')}}">
                    <i class="fa fa-plus"></i>
                </a></th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $index=>$a)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $a->name }}</td>
                <td>{{ $a->divname }}</td>
                <td>
                    <a class="btn btn-sm btn-success button1" href="{{ url('occupation/' . $a->id . '/edit') }}">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a class="btn btn-sm btn-danger button1" href="{{ url('occupation/' . $a->id . '/delete') }}" onclick="return confirm('tenan ki meh dihapus?')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>
@endsection
