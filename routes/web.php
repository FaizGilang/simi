<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontpage
Route::get('/', function () {
    return view('welcome');
});
//auth
Auth::routes();
//home
Route::get('/home', 'HomeController@index')->name('home');

//user{
//index
Route::get('/user', 'UserController@index');
Route::get('/user/reset', 'UserController@reset');
Route::post('/user/reset', 'UserController@saveReset');
Route::get('/user/resetpassword/{id}', 'UserController@resetPassword');
//add
Route::get('/user/change','UserController@change');
Route::post('/user/change','UserController@saveChange');
Route::get('/user/add','UserController@create');
Route::post('/user/add','UserController@store');
//edit
Route::get('/user/{id}/edit','UserController@edit');
Route::post('/user/{id}/edit','UserController@update');
//delete
Route::get('/user/{id}/delete','UserController@destroy');
//}

//ocibindbyname()	cupation{
//index
Route::get('/occupation', 'OccupationController@index');
//add
Route::get('/occupation/add', 'OccupationController@create');
Route::post('/occupation/add', 'OccupationController@store');
//edit
Route::get('/occupation/{id}/edit', 'OccupationController@edit');
Route::post('/occupation/{id}/edit', 'OccupationController@update');
//deletee
Route::get('/occupation/{id}/delete','OccupationController@destroy');
//}

//division{
//index
Route::get('/division', 'DivisionController@index');
//add
Route::get('/division/add', 'DivisionController@create');
Route::post('/division/add', 'DivisionController@store');
//edit
Route::get('/division/{id}/delete','DivisionController@destroy');
Route::get('/division/{id}/edit','DivisionController@edit');
Route::post('/division/{id}/edit','DivisionController@update');
//}

//item{
//index
Route::get('/item', 'ItemController@index');
Route::get('/item/delete/{id}', 'ItemController@destroy');
//add
Route::get('/item/add', 'ItemController@create');
Route::post('/item/add', 'ItemController@store');
Route::get('/item/edit/{id}', 'ItemController@edit');
Route::post('/item/edit/{id}', 'ItemController@update');
//}

//item category{
//index
Route::get('/itemcategory', 'ItemCategoryController@index');
//add
Route::get('/itemcategory/add', 'ItemCategoryController@create');
Route::post('/itemcategory/add', 'ItemCategoryController@store');
Route::get('/itemcategory/delete/{id}', 'ItemCategoryController@destroy');
Route::get('/itemcategory/edit/{id}', 'ItemCategoryController@edit');
Route::post('/itemcategory/edit/{id}', 'ItemCategoryController@update');
//}

//procurement{
//index
Route::get('/procurement', 'ProcurementController@index');
Route::get('/procurement/additem/{id}/{jml}', 'ProcurementController@addItem');

//add
Route::get('/procurement/add', 'ProcurementController@create');
Route::get('/procurement/save', 'ProcurementController@store');
//accept
Route::get('/procurement/{id}/accept', 'ProcurementController@accept');
Route::get('/procurement/{id}/fund', 'ProcurementController@fund');
Route::post('/procurement/{id}/fund', 'ProcurementController@save');
//detail
Route::get('/procurement/{id}/detail', 'ProcurementController@show');

//cart
Route::resource('cart', 'CartController');
Route::delete('emptyCart', 'CartController@emptyCart');
Route::post('switchToWishlist/{id}', 'CartController@switchToWishlist');

Route::resource('wishlist', 'WishlistController');
Route::delete('emptyWishlist', 'WishlistController@emptyWishlist');
Route::post('switchToCart/{id}', 'WishlistController@switchToCart');
//}

Route::post('posts/changeStatus', array('as' => 'changeStatus', 'uses' => 'ProcurementController@changeStatus'));

//funds{
Route::get('/fund/', 'FundController@index');
Route::get('/fund/add', 'FundController@create');
Route::post('/fund/add', 'FundController@store');
Route::get('/fund/{id}/active', 'FundController@active');
//}

//funddis{
Route::get('/funddis/', 'FundDistributionController@index');
Route::get('/funddis/add', 'FundDistributionController@create');
Route::post('/funddis/add', 'FundDistributionController@store');
Route::get('/funddis/data/{id}', 'FundDistributionController@data');
//}

//warehouse{
Route::get('/warehouse', 'WarehouseController@index');
Route::get('/warehouse/detail/{id}', 'WarehouseController@show');
Route::get('/warehouse/detail1/{id}', 'WarehouseController@show1');
Route::get('/warehouse/detail2/{id}', 'WarehouseController@show2');
Route::get('/warehouse/all', 'warehouseController@indexAll');
Route::get('/warehouse/del', 'warehouseController@indexDel');
Route::get('/warehouse/edit/{id}', 'WarehouseController@edit');
Route::post('/warehouse/edit/{id}', 'WarehouseController@update');
Route::get('/warehouse/delete/{id}', 'WarehouseController@delete');
Route::get('/warehouse/delete1/{id}', 'WarehouseController@delete1');
Route::post('/warehouse/delete/{id}', 'WarehouseController@saveDelete');
Route::post('/warehouse/delete1/{id}', 'WarehouseController@saveDelete1');
Route::get('/warehouse/accept/{id}', 'WarehouseController@confirm');
Route::get('/warehouse/delyear', 'warehouseController@indexDelYear');
Route::get('/warehouse/delDetail/{year}', 'warehouseController@showDel');
Route::get('/warehouse/print/{year}', 'WarehouseController@print');

Route::get('/arrival', 'ItemArrivalController@index');
Route::get('/arrival/add', 'ItemArrivalController@create');
Route::post('/arrival/add', 'ItemArrivalController@store');
Route::get('/arrival/edit/{id}', 'ItemArrivalController@edit');
Route::post('/arrival/edit/{id}', 'ItemArrivalController@update');
Route::get('/arrival/delete/{id}', 'ItemArrivalController@destroy');
Route::get('/arrival/delete1/{id}', 'ItemArrivalController@destroy1');
Route::get('/arrival/verify/{id}', 'ItemArrivalController@verify');
Route::get('/arrival/detail/{id}', 'ItemArrivalController@show');
Route::get('/arrival/print/{id}', 'ItemArrivalController@print');

Route::get('/distribution', 'DistributionController@index');
Route::get('/distribution/add', 'DistributionController@create');
Route::post('/distribution/add', 'DistributionController@store');
Route::get('/distribution/delete/{id}', 'DistributionController@destroy');
Route::get('/distribution/delete1/{id}', 'DistributionController@destroy1');
Route::get('/distribution/edit/{id}', 'DistributionController@edit');
Route::post('/distribution/edit/{id}', 'DistributionController@update');
Route::get('/distribution/redistribution', 'DistributionController@redistribute');
Route::get('/distribution/data/{id}', 'DistributionController@data');
Route::post('/distribution/redistribution', 'DistributionController@redistributeSave');
Route::get('/distribution/detail/{id}', 'DistributionController@show');
Route::get('/distribution/verify/{id}', 'DistributionController@verify');
Route::get('/distribution/print/{id}', 'DistributionController@print');

//loan{
Route::get('/loan', 'LoanController@index');
Route::get('/loan/add', 'LoanController@create');
Route::post('/loan/add', 'LoanController@store');
Route::get('/loan/detail/{id}', 'LoanController@show');
Route::get('/loan/verify/{id}', 'LoanController@verify');
Route::get('/warehouse/all', 'warehouseController@indexall');
//Route::get('/loan/getnumber/{id}', 'LoanController@data');

//}