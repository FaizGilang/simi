<?php

namespace App\Http\Controllers;

use App\User;
use App\Occupation;
use App\Division;
use DB;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use App\Http\Requests\AddUserRequest;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('users')
                ->join('occupations', 'users.occupation', '=', 'occupations.id')
                ->join('divisions', 'occupations.division_id', '=', 'divisions.id')
                ->select('occupations.name as occname', 'users.*', 'divisions.name as divname')
                ->get();
        return view('admin.user.index')
        ->with('menu', 'user')
        ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = occupation::where('division_id','!=','1')->get();
        return view('admin.user.add')
        ->with('menu', 'user')           
        ->with('occupation', $data);           
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
        //
        $data = new User;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt('katakunci');
        $data->occupation = $request->occupation;
        $data->reset = '0';
        $data->save();

        Alert::success('Data Pengguna berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/user')
        ->with('menu','user');  
    }

     public function change()
    {
        return view('admin.user.change')
        ->with('menu', 'home');           
    }
    public function saveChange(Request $request)
    {
        $data = User::find(Auth::user()->id);
        $data->password= bcrypt($request->password);
        $data->save();
       return redirect('/home')
        ->with('menu','user');            
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function reset(User $user)
    {
         $data = occupation::where('division_id','!=','1')->get();
        return view('admin.user.reset');
    }
    public function saveReset(Request $request )
    {
         $data = User::where("email",$request->email)->get()->first();
         $reset = User::find($data->id);
         $reset->reset = 1;
         $reset->save();
        return redirect('/login');
    }

    public function resetPassword($id )
    {
         $reset = User::find($id);
         $reset->password = bcrypt('katakunci');
         $reset->reset = 0;
         $reset->save();
        return redirect('/user');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DB::table('users')
        ->join('occupations', 'users.occupation', '=', 'occupations.id')
        ->select('occupations.name as occname', 'users.*')
        ->where('users.id','=',$id)
        ->get()->first();
        $data1 = Occupation::all();
        return view('admin.user.edit')
        ->with('menu','user')
        ->with('user',$data)
        ->with('occupation',$data1);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  User::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->occupation = $request->occupation;
        $data->save();

        Alert::success('Data Pengguna berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/user')
        ->with('menu','user');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = User::find($id);
        $data->delete();

          Alert::success('Data Pengguna berhasil Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('user/');
    }
}
