<?php

namespace App\Http\Controllers;

use App\User;
use App\Occupation;
use App\Division;
use App\Item;
use App\ItemCategory;
use DB;
use File;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Alert;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
    {
        $data = DB::table('items')
                ->join('item_categories', 'item_categories.id', '=', 'items.category_id')
                ->select('item_categories.name as itemname', 'items.*')
                ->get();
        return view('admin.item.index')
        ->with('menu', 'item')
        ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = ItemCategory::all();  
        return view('admin.item.add')
        ->with('category',$data)
        ->with('menu','item');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new Item;
        $data->name = $request->name;
        $data->category_id = $request->category;
        $data->specification = $request->specification;
        $data->code = $request->code;
        $data->save();
        Alert::success('Data Barang berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/item')
        ->with('menu','item');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data1  = Item::join('item_categories','item_categories.id','=','items.category_id')
        ->where('items.id',$id)
        ->select('items.*','item_categories.id as idcat','item_categories.name as namecat')
        ->get()->first();
        $data = ItemCategory::all();  
        return view('admin.item.edit')
        ->with('category',$data)
        ->with('data1',$data1)
        ->with('menu','item');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  
        $data = Item::find($id);
        $data->name = $request->name;
        $data->category_id = $request->category;
        $data->specification = $request->specification;
        $data->code = $request->code;
        $data->save();

               Alert::success('Data Barang berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $data = Item::find($id);
        $data->delete();

          Alert::success('Data Barang berhasil Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('item/');
    }
}
