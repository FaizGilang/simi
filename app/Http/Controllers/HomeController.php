<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Item;
use App\Warehouse;
use App\ItemArrival;
use App\ItemArrivalDetail;
use App\ItemCategory;
use App\Division;
use Auth;
use DB;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$all = Warehouse::count();
		$active =warehouse::where('status','!=','deleted')->count();
		$distibuted =warehouse::where('user_id','!=',0)->count();
		$proces =warehouse::where('status','=','deleted')->count();
        return view('home')
        ->with ('all',$all)
        ->with ('active',$active)
        ->with ('dis',$distibuted)
        ->with ('proc',$proces)
        ->with ('menu','home');
    }

        public function __construct()
    {
        $this->middleware('auth');
    }

}
