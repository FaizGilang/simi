<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fund;
use App\Division;
use App\FundDistribution;
use DB;

class FundDistributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = DB::table('fund_distributions')
        ->join('divisions','fund_distributions.division_id','=','divisions.id')
        ->select('fund_distributions.*','divisions.name')
        ->get();
        return view('admin.funddistribution.index')
        ->with('data',$data)
        ->with('menu','fund'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = Fund::all();
        return view('admin.funddistribution.add')
        ->with('menu','fund')->with('year', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $lastyear = FundDistribution::where('division_id','=',$request->division)
        ->where('year','=',$request->year-1)->get()->first();
        if(empty($lastyear)){
            $data = new FundDistribution;
            $data->division_id = $request->division;
            $data->amount = $request->amount;
            $data->year = $request->year;
            $data->used = 0;
            $data->last_year_leftover = 0;
            $data->save();
        }else{
            $data = new FundDistribution;
            $data->division_id = $request->division;
            $data->amount = $request->amount;
            $data->year = $request->year;
            $data->used = 0;
            $data->last_year_leftover = $lastyear->last_year_leftover+$lastyear->amount-$lastyear->used;
            $data->save();
        }

        

        $fund = Fund::where('year','=',$request->year)->get()->first();
        $fund->used = $fund->used + $request->amount;
        $fund->save();

        return redirect('/funddis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function data($id)
    {
        //

        $funddis = FundDistribution::where('year','=',$id)->pluck('division_id');
        $data = Division::whereNotIn('id', $funddis)
        ->where('id', '!=',1)
        ->where('id', '!=',2)
        ->pluck('name','id');
        return json_encode($data);
    }
}
