<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Warehouse;
use App\ItemArrival;
use App\ItemArrivalDetail;
use App\ItemCategory;
use App\Division;
use App\Distribution;
use App\DistributionDetail;
use App\User;
use App\Loan;
use App\LoanDetail;
use Auth;
use DB;
use Carbon\Carbon;
use Alert;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = Loan::join('users','users.id','loans.lender')
         ->select('loans.*','users.name')
         ->get();
        return view('admin.loan.index')
        ->with('data',$data)
        ->with('menu','loan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data1 = User::all();
        $data = Item::all();
        return view ('admin.loan.add')
        ->with('item',$data)
        ->with('data',$data1)
        ->with('menu','loan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $detail = new Loan;
        $detail->lender=$request->lender;
        $detail->borrowdate=Carbon::now();
        $detail->returndate=Carbon::now()->addMonths(3);
        $detail->status="unverified";
        $detail->save();

        foreach ($request->field_name as $key=>$iter) {
            $data = new LoanDetail;
            $data->loan_id=$detail->id;
            $data->item_id=$iter;
            $data->number=$request->field_number[$key];
            $data->save();
        }
        Alert::success('Data Peminjaman berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/loan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Loan::join('users', 'users.id', 'loans.lender')
        ->where('loans.id',$id)
        ->get()->first();
 
        $data1= LoanDetail::where('loan_details.loan_id', $id)
        ->join('items','items.id','loan_details.item_id')
        ->get();
        return view ('admin.loan.show')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','loan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

      public function data($id)
    {
        //
        $data = Warehouse::where('item_id',$id)->pluck('number','id');
        return json_encode($data);
    }

      public function verify($id)
    {
        //
        $data = Loan::find($id);
        $data->status = 'verified';
        $data->save();
        return redirect('/loan');
    }
}
