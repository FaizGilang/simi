<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Item;
use App\Warehouse;
use App\ItemArrival;
use App\ItemArrivalDetail;
use App\ItemCategory;
use App\Division;
use Carbon\Carbon;
use Auth;
use DB;
use Alert;
use PDF;

class ItemArrivalController extends Controller
{
    //
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $data = ItemArrival::all();
        return view('admin.arrival.index')
        ->with('data',$data)
        ->with('menu','document');
    }

    public function create()
    {
        //
        $data = Item::all();
        return view ('admin.arrival.add')
        ->with('item',$data)
        ->with('menu','document');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $detail = new ItemArrival;
        $detail->year=$request->year;
        $detail->status="unverified";
        $detail->save();

        foreach ($request->field_name as $key=>$iter) {
            $data = new ItemArrivalDetail;
            $data->item_id=$iter;
            $data->item_arrival_id=$detail->id;
            $data->fund_source=$request->field_source[$key];
            $data->amount=$request->field_amount[$key];
            $data->vendor=$request->field_vendor[$key];
            $data->save();
        }

        Alert::success('Data Berita Acara Penerimaan Barang berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/arrival');
        
    }
        public function verify($id)
    {
        //
        $arrival = ItemArrivalDetail::where('item_arrival_id',$id)->get();
        
        $arrival_year = ItemArrival::find($id);
        $arrival_year->status ="verified";
        $arrival_year->save();

        $year = Carbon::createFromFormat('Y', $arrival_year->year); 

        foreach ($arrival as $arrival) {
        for ($x = 0; $x < $arrival->amount; $x++) {
            # code...

            // $cat = ItemArrival::find($arrival->id);
            // $cat->amount = $cat->amount-1;
            // $cat->save();

            if($arrival->fund_source == 1){
                $source = "N";
            }elseif($arrival->fund_source == 2){
                $source = "D";
            }else{
                $source = "L";
            }

            if($arrival->amount < 10){
                $from = "00".$arrival->amount;
            }elseif($arrival->amount < 100){
                $from = "0".$arrival->amount;
            }else{
                $from = $arrival->amount;
            }
            $item = Item::find($arrival->item_id);

            $code = ItemCategory::find($item->category_id);
            
       
            $number = Warehouse::where('item_id',$arrival->item_id)
            ->where('fund_source',$arrival->fund_source)
            ->where('year',$year->format('Y'))
            ->count();
            $a=$number+1;
            if($a< 10){
                $order = "00".$a;
            }elseif($a< 100){
                $order = "0".$a;
            }else{
                $order = $a;
            }


            $data = new Warehouse;
            $data->item_id = $arrival->item_id;
            //urutan-dari keseluruhan
            $data->number = "AKN/".$source.$year->format('Y')."/".$code->code.$item->code.$order."-".$from;
            $data->fund_source = $arrival->fund_source;
            $data->year = $year->format('Y');
            $data->deleteyear = "-";
            $data->user_id = "0";
            $data->arrival_doc = $id;
            $data->distribute_doc = "0";
            $data->condition = "Baik";
            $data->status = "active";
            $data->vendor = $arrival->vendor;
            $data->location = "Gudang";
            $data->description = "-";
            $data->save();

        }
        }
        return redirect('/warehouse');
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

     $data = ItemArrival::find($id);

        $data1= ItemArrivalDetail::where('item_arrival_details.item_arrival_id', $id)
        ->join('items','items.id','item_arrival_details.item_id')
        ->get();
        return view ('admin.arrival.show')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','document');
    }

    public function edit($id)
    {
        //
        $data = ItemArrival::find($id);
        $data1= ItemArrivalDetail::where('item_arrival_details.item_arrival_id', $id)
        ->join('items','items.id','item_arrival_details.item_id')
        ->select('items.*','item_arrival_details.*','item_arrival_details.id as iddetail')
        ->get();
        $data2 = Item::all();
        return view('admin.arrival.edit')
        ->with('menu','document')
        ->with('item',$data2)
        ->with('data',$data)
        ->with('data1',$data1);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
        $cek = ItemArrivalDetail::where('item_arrival_id',$id)->get();
        $data1 = ItemArrival::find($id);
        $data1->year = $request->year;
        $data1->save();
        foreach ($cek as $a) {
            $delete = ItemArrivalDetail::find($a->id);
            $delete->delete();
        }
       foreach ($request->field_name as $key=>$iter) {
            $data = new ItemArrivalDetail;
            $data->item_id=$iter;
            $data->item_arrival_id=$id;
            $data->fund_source=$request->field_source[$key];
            $data->amount=$request->field_amount[$key];
            $data->vendor=$request->field_vendor[$key];
            $data->save();
        }

        Alert::success('Data Berita Acara Penerimaan Barang berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/arrival');

    }

    public function destroy($id)
    {

        $cek = ItemArrival::find($id);
        $cek->delete();
        
        $item = ItemArrivalDetail::where('item_arrival_id',$id)->get();

        foreach ($item as $key ) {
            $hapus = ItemArrivalDetail::find($key->id);
            $hapus->delete();
        }
        Alert::success('Berita Acara Penerimaan Barang berhasil Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('/arrival');
        
    }

    public function destroy1($id)
    {
        $cek = ItemArrivalDetail::find($id);
        $hitung = ItemArrivalDetail::where('item_arrival_id',$cek->item_arrival_id)->count();
        if($hitung == 1){
            $cek->delete();
            $hapus = ItemArrival::find($cek->item_arrival_id);
            $hapus->delete();

            Alert::success('Berita Acara Penerimaan Barang berhasil Dihapus', 'Berhasil')->autoclose(2000);
            return redirect('/arrival');
        }else{
            $cek->delete();
            return redirect('/arrival/edit/'.$cek->item_arrival_id);
        }
    }

    public function print($id)
    {
        //
        $user = User::where('occupation',10)->get()->first();
        $user1 = User::where('occupation',7)->get()->first();
        $item = ItemArrivalDetail::join('items','items.id','item_arrival_details.item_id')
        ->where('item_arrival_details.item_arrival_id',$id)->get();
        $pdf = PDF::loadView('admin.arrival.print',compact('item','user','user1'));
         
        return $pdf->stream('Arrival-'.$id.'.pdf');
    }
}
