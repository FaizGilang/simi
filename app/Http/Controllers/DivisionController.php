<?php

namespace App\Http\Controllers;

use App\Division;
use App\User;
use Illuminate\Http\Request;
use Alert;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = Division::all();
        return view('admin.division.index')
        ->with('menu', 'user')
        ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.division.add')
        ->with('menu', 'user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $division = new Division;
        $division->name = $request->name;
        $division->save();
        Alert::success('Data Divisi berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/division')->with('menu', 'user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function show(Division $division)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Division::find($id);
         return view('admin.division.edit')
        ->with('menu', 'user')
        ->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $division = Division::find($id);
        $division->name = $request->name;
        $division->save();
        Alert::success('Data Divisi berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/division')->with('menu', 'user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        //
        $data = Division::find($id);
        $data->delete();

        Alert::success('Data Divisi Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('division/');
    }
}
