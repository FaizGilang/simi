<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Item;
use App\Warehouse;
use App\ItemArrival;
use App\ItemArrivalDetail;
use App\ItemCategory;
use App\Division;
use Carbon\Carbon;
use Auth;
use DB;
use Alert;
use PDF;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->occupation != 7|| Auth::user()->occupation !=10){
             $data=Warehouse::join('items','items.id','=','warehouses.item_id')
        ->where('warehouses.user_id','=',0)
        ->groupBy('items.id')->select('items.name',DB::raw('SUM(IF(items.id     IS NULL, 0, 1)) AS amount'),'items.id')
        ->get();

        return view('admin.warehouse.index')
        ->with('data',$data)
        ->with('menu','warehouse');
    }else
        {
        $data=Warehouse::join('items','items.id','=','warehouses.item_id')
        ->where('warehouses.user_id','=',0)
        ->groupBy('items.id')->select('items.name',DB::raw('SUM(IF(items.id     IS NULL, 0, 1)) AS amount'),'items.id')
        ->get();

        return view('admin.warehouse.index')
        ->with('data',$data)
        ->with('menu','warehouse');
    }
    }

     //
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAll()
    {
        //
        $data = DB::table('warehouses')
        ->join('items','items.id','=','warehouses.item_id')
        ->join('users','users.id','=','warehouses.user_id')
        ->where('warehouses.status','!=','deleted')
        ->select('warehouses.*','items.name','users.name as divname')
        ->get(); 
        $data1 = DB::table('warehouses')
        ->join('items','items.id','=','warehouses.item_id')
        ->where('warehouses.user_id', 0)
        ->where('warehouses.status','!=','deleted')
        ->select('warehouses.*','items.name')
        ->get();
        return view('admin.warehouse.indexall')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','warehouse');

    }
    public function indexDel()
    {
        //
        $data = DB::table('warehouses')
        ->join('items','items.id','=','warehouses.item_id')
        ->join('users','users.id','=','warehouses.user_id')
        ->where('warehouses.status','=','deleted')
        ->select('warehouses.*','items.name','users.name as divname')
        ->get(); 
        $data1 = DB::table('warehouses')
        ->join('items','items.id','=','warehouses.item_id')
        ->where('warehouses.user_id', 0)
        ->where('warehouses.status','=','deleted')
        ->select('warehouses.*','items.name')
        ->get();
        return view('admin.warehouse.indexdel')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','warehouse');
    }
     public function indexDelYear()
    {
        //
            $data=Warehouse::join('items','items.id','=','warehouses.item_id')
        ->where('warehouses.status','=','deleted')
        ->groupBy('warehouses.deleteyear')->select('warehouses.deleteyear as name',DB::raw('SUM(IF(warehouses.deleteyear     IS NULL, 0, 1)) AS amount'))
        ->get();
    
        return view('admin.warehouse.indexdelyear')
        ->with('data',$data)
        ->with('menu','document');
    
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = Item::all();
        return view ('admin.arrival.add')
        ->with('item',$data)
        ->with('menu','document');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $detail = new ItemArrival;
        $detail->year=$request->year;
        $detail->status="unverified";
        $detail->save();

        foreach ($request->field_name as $key=>$iter) {
            $data = new ItemArrivalDetail;
            $data->item_id=$iter;
            $data->item_arrival_id=$detail->id;
            $data->fund_source=$request->field_source[$key];
            $data->amount=$request->field_amount[$key];
            $data->vendor=$request->field_vendor[$key];
            $data->save();
        }

        Alert::success('Data Berita Acara Penerimaan Barang berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/arrival');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Item::find($id);

        $data1= warehouse::join('items','items.id','warehouses.item_id')
        ->where('items.id', $id)
        ->where('warehouses.user_id', 0)
        ->get();
        return view ('admin.warehouse.show')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','warehouse');
    }
     public function show1($id)
    {
        //

        $data1= warehouse::join('items','items.id','warehouses.item_id')
        ->where('warehouses.id', $id)
        ->get()->first();
        return view ('admin.warehouse.show1')
        ->with('data1',$data1)
        ->with('menu','warehouse');
    }
     public function show2($id)
    {
        //
        $data1= warehouse::join('items','items.id','warehouses.item_id')
        ->join('users','users.id','warehouses.user_id')
        ->where('warehouses.id', $id)
        ->select('items.*','warehouses.*','users.name as username')
        ->get()->first();
        return view ('admin.warehouse.show2')
        ->with('data1',$data1)
        ->with('menu','warehouse');
    }
     public function showDel($year)
    {
        //
        $data = $year;

        $data1= warehouse::join('items','items.id','warehouses.item_id')
        ->where('warehouses.status', 'deleted')
        ->where('warehouses.deleteyear', $year)
        ->get();
        return view ('admin.warehouse.showDel')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','document');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
           $data = warehouse::join('items','items.id','warehouses.item_id')
           ->where('warehouses.id',$id)->get()->first();


        return view ('admin.warehouse.edit')
        ->with('data',$data)
        ->with('menu','document');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
            $data = Warehouse::find($id);
            $data->location=$request->location;
            $data->save();
         Alert::success('Data Inventaris Barang berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/warehouse/all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function distribute()
    {
        //
        $data = DB::table('item_arrival_details')
        ->join('items','items.id','=','item_arrival_details.item_id')
        ->select('item_arrival_details.*','items.name')
        ->get();
        $div = Division::all();
        return view ('admin.warehouse.distribute')
        ->with('item',$data)
        ->with('division',$div)
        ->with('menu','warehouse');
    }

    public function confirm( $id)
    {
        $year = Carbon::now();
        $data = Warehouse::find($id);
        $data->status = 'deleted';
        $data->deleteyear =  $year->format('Y');
        $data->save();
        return redirect('/warehouse/all');
        
    }



     public function delete($id)
    {
        //
        $data = Warehouse::find($id);

        return view ('admin.warehouse.delete')
        ->with('data',$data)
        ->with('menu','warehouse');
    }

      public function saveDelete(Request $request, $id)
    {
        //
        $data = Warehouse::find($id);
        $data->status = 'delete';
        $data->description = $request->description;
        $data->save();
        return redirect('/warehouse/all');
    }

         public function delete1($id)
    {
        //
        $data = Warehouse::find($id);

        return view ('admin.warehouse.delete1')
        ->with('data',$data)
        ->with('menu','warehouse');
    }

      public function saveDelete1(Request $request, $id)
    {
        //
        $data = Warehouse::find($id);
        if($request->description == "1"){

        $data->status = 'active';
        }
        else{
        $data->status = 'neardeleted';
        }
        $data->condition = $request->condition;
        
        $data->save();
        return redirect('/warehouse/all');
    }

     public function print($year)
    {
        //
        $user = User::where('occupation',10)->get()->first();
        $user1 = User::where('occupation',7)->get()->first();
        $item = Warehouse::join('items','items.id','warehouses.item_id')
        ->where('warehouses.status','deleted')
        ->where('warehouses.deleteyear',$year)->get();
        $pdf = PDF::loadView('admin.warehouse.print',compact('item','user','user1'));
         
        return $pdf->stream('Deletment-'.$year.'.pdf');
    }
}
