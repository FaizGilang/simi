<?php

namespace App\Http\Controllers;

use App\Fund;
use App\FundSource;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Fund::all();
        return view('admin.fund.index')
        ->with('data',$data)
        ->with('menu','fund');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.fund.add')
        ->with('menu','fund');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $total = 0;
         foreach($request->field_price as $add) {
            $total=$total+$add; 
        }

        $fund = new Fund;
        $fund->amount = $total;
        $fund->used = 0;
        $fund->year = $request->year;
        $fund->status = 'new';
        $fund->save();

        foreach ($request->field_name as $key=>$iter) {
            $fundSource = new FundSource;
            $fundSource->fund_id = $fund->id;
            $fundSource->source = $iter;
            $fundSource->amount = $request->field_price[$key];
            $fundSource->save();
        }

        Alert::success('Data Persediaan berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
      
        return redirect('/fund');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function active($id)
    {
        //
        $data = Fund::find($id);
        $data->status = 'active';
        $data->save();

        $fund = Fund::where('year','=',$data->year-1)->get()->first();
        
        if(empty($fund)){

        }else{
          $fund->status = 'closed';
          $fund->save();
        }
        return redirect('/fund');
        
    }
}
