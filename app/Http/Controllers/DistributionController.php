<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Warehouse;
use App\ItemArrival;
use App\ItemArrivalDetail;
use App\ItemCategory;
use App\Division;
use App\Distribution;
use App\DistributionDetail;
use App\User;
use Carbon\Carbon;
use Auth;
use DB;
use Alert;
use PDF;

class DistributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = Distribution::join('users','users.id','=','distributions.to' )
         ->select('distributions.id','distributions.status','users.name')
         ->get();
        return view('admin.distribution.index')
        ->with('data',$data)
        ->with('menu','document');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $data=Warehouse::join('items','items.id','=','warehouses.item_id')
        ->where('warehouses.user_id','=',0)
        ->groupBy('items.id')->select('items.name',DB::raw('SUM(IF(items.id     IS NULL, 0, 1)) AS amount'),'items.id')
        ->get();

        $data1 = User::all();

        return view ('admin.distribution.add')
        ->with('item',$data)
        ->with('data',$data1)
        ->with('menu','document');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detail = new Distribution;
        $detail->from=Auth::user()->id;
        $detail->to=$request->to;
        $detail->status="unverified";
        $detail->save();

        foreach ($request->field_name as $key=>$iter) {
            $data = new DistributionDetail;
            $data->item_id=$iter;
            $data->distribution_id=$detail->id;
            $data->amount=$request->field_amount[$key];
            $data->save();
        }
        Alert::success('Data Serah Terima berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/distribution');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Distribution::where('distributions.id',$id)
        ->join('users', 'users.id', 'distributions.to')
        ->get()->first();

        $data1= DistributionDetail::where('distribution_details.distribution_id', $id)
        ->join('items','items.id','distribution_details.item_id')
        ->get();
        return view ('admin.distribution.show')
        ->with('data',$data)
        ->with('data1',$data1)
        ->with('menu','document');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data = Distribution::where('distributions.id',$id)
        ->join('users', 'users.id', 'distributions.to')
        ->select('users.id as iduser','users.name', 'distributions.*')
        ->get()->first();
        $data1= DistributionDetail::where('distribution_details.distribution_id', $id)
        ->join('items','items.id','distribution_details.item_id')
        ->select('items.*','distribution_details.*','distribution_details.id as iddetail')
        ->get();
        $data2 = Item::all();
        $data3 = User::all();
        return view('admin.distribution.edit')
        ->with('menu','document')
        ->with('item',$data2)
        ->with('data',$data)
        ->with('data2',$data3)
        ->with('data1',$data1);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $cek = DistributionDetail::where('distribution_id',$id)->get();
        $data1 = Distribution::find($id);
        $data1->from=Auth::user()->id;
        $data1->to=$request->to;
        $data1->save();
        foreach ($cek as $a) {
            $delete = DistributionDetail::find($a->id);
            $delete->delete();
        }
         foreach ($request->field_name as $key=>$iter) {
            $data = new DistributionDetail;
            $data->item_id=$iter;
            $data->distribution_id=$id;
            $data->amount=$request->field_amount[$key];
            $data->save();
        }

        Alert::success('Data Berita Acara Serah Terima Barang berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/distribution');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
    {

        $cek = Distribution::find($id);
        $cek->delete();
        
        $item = DistributionDetail::where('distribution_id',$id)->get();

        foreach ($item as $key ) {
            $hapus = DistributionDetail::find($key->id);
            $hapus->delete();
        }
        Alert::success('Berita Acara Serah Terima Barang berhasil Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('/distribution');
    }

    public function destroy1($id)
    {
        $cek = DistributionDetail::find($id);

        $hitung = DistributionDetail::where('distribution_id',$cek->distribution_id)->count();
        if($hitung == 1){
            $cek->delete();
            $hapus = Distribution::find($cek->distribution_id);
            $hapus->delete();

            Alert::success('Berita Acara Serah Terima Barang berhasil Dihapus', 'Berhasil')->autoclose(2000);
            return redirect('/distribution');
        }else{
            $cek->delete();
            return redirect('/distribution/edit/'.$cek->distribution_id);
        }
    }

    public function verify($id)
    {
        //
        $distribution = Distribution::find($id);

        $detail = DistributionDetail::where('distribution_id',$distribution->id)->get();

        foreach ($detail as $key ) {
            # code...
            for ($i=0; $i <$key->amount ; $i++) { 
                # code...
                $warehouse = Warehouse::where('user_id',0)
                ->where('item_id',$key->item_id)->get()->first();
                $warehouse->user_id=$distribution->to;
                $warehouse->distribute_doc = $id;
                $warehouse->save();
            }
        }
            $distribution->status ='verified';
            $distribution->save();
            return redirect('/distribution');
    }

    public function print($id)
    {
        //
        $help = Distribution::find($id);
        $from = User::find($help->from);
        $to = User::find($help->to);
        $user = User::where('occupation',7)->get()->first();
        $item = DistributionDetail::join('items','items.id','distribution_details.item_id')
        ->where('distribution_details.distribution_id',$id)->get();
        $pdf = PDF::loadView('admin.distribution.print',compact('item','from','to','user'));
         
        return $pdf->stream('Distribution-'.$id.'.pdf');
    }

    public function redistribute()
    {
        //

        $data = User::all();

        return view ('admin.distribution.redistribute')
        ->with('data',$data)
        ->with('menu','document');
    }

    public function redistributeSave(Request $request)
    {
        if(empty($request->field_id)){
            Alert::error('Silahkan Pilih Barang', 'Maaf')->autoclose(2000);
            return back();
        }
        else{
            foreach ($request->field_id as $key=>$iter){
            $redistribute = Warehouse::find($iter);
            $redistribute->user_id=$request->to;
            $redistribute->save();
            }

            $detail = new Distribution;
            $detail->from=Auth::user()->id;
            $detail->to=$request->to;
            $detail->status="verified";
            $detail->save();

            foreach ($request->field_id as $key=>$iter) {
                $item = Warehouse::find($iter);

                $data = new DistributionDetail;
                $data->item_id=$item->item_id;
                $data->distribution_id=$detail->id;
                $data->amount=1;
                $data->save();
            }

            Alert::success('Data Serah Terima berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
            return redirect('/distribution');
        }

}   

    public function data($id)
    {
            $data = DB::table('warehouses')->where('warehouses.user_id',$id)
            ->join('items','items.id','warehouses.item_id')
            ->select('items.name','items.id','warehouses.number','warehouses.id')
            ->get(); 
            return json_encode($data);
    }
}
