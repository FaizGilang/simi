<?php

namespace App\Http\Controllers;

use App\Occupation;
use App\Division;
use App\User;
use DB;
use Illuminate\Http\Request;
use Alert;

class OccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$data = DB::table('occupations')
        ->join('divisions', 'occupations.division_id', '=', 'divisions.id')
        ->select('occupations.*', 'divisions.name as divname')
        ->get();
        return view('admin.occupation.index')
        ->with('menu', 'user')
        ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = Division::all();
        return view('admin.occupation.add')
        ->with('menu', 'user')
        ->with('division',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$occupation = new Occupation;
        $occupation->name = $request->name;
        $occupation->division_id = $request->occupation;
        $occupation->save();

        Alert::success('Data Jabatan berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/occupation')
        ->with('menu','user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
    public function show(Occupation $occupation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DB::table('occupations')
        ->join('divisions', 'divisions.id', '=', 'occupations.division_id')
        ->select('divisions.name as divname', 'occupations.*')
        ->where('occupations.id','=',$id)
        ->get()->first();;
        $data1 = Division::all();
        return view('admin.occupation.edit')
        ->with('menu','user')
        ->with('data',$data)
        ->with('data1',$data1);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $occupation = Occupation::find($id);
        $occupation->name = $request->name;
        $occupation->division_id = $request->occupation;
        $occupation->save();

        Alert::success('Data Jabatan berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/occupation')
        ->with('menu','user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        //
        $data = Occupation::find($id);
        $data->delete();

      
         Alert::success('Data Jabatan Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('occupation/');
    }
}
