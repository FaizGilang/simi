<?php

namespace App\Http\Controllers;

use App\Procurement;
use App\ProcurementDetail;
use App\Item;
use \Cart as Cart;
use Illuminate\Http\Request;
use Auth;
use DB;
class ProcurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('procurements')
        ->join('users','users.id','=','procurements.applicant')
        ->join('occupations','occupations.id','=', 'users.occupation')
        ->join('divisions','divisions.id','=','occupations.division_id')
        ->select('procurements.*','users.name','divisions.name as division')
        ->get();
        return view('admin.procurement.index')
        ->with('menu','procurement')
        ->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = Item::all();
        return view('admin.procurement.add')
        ->with('menu','procurement')
        ->with('item',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = new Procurement;
        $data->applicant=Auth::user()->id;
        $data->status="applied";
        $data->save();
   
        foreach (Cart::content() as $item) {
                    $detail = new ProcurementDetail();
                    $detail->procurement_id = $data->id;
                    $detail->item_id = $item->id;
                    $detail->amount = $item->qty;
                    $detail->save();
                }
        Cart::destroy();
        Alert::success('Data Pengguna berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/procurement');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Procurement  $procurement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         //
        $data = DB::table('procurements')
        ->join('users','users.id','=','procurements.applicant')
        ->join('occupations','users.occupation','=','occupations.id')
        ->join('divisions','users.division','=','divisions.id')
        ->select('procurements.*','users.name','divisions.name as divname','occupations.name as occuname')
        ->get()->first();

        $procurement = DB::table('procurements')
        ->join('procurement_details','procurement_details.procurement_id','=', 'procurements.id')
        ->select('procurements.*','procurement_details.*')
        ->where('procurement_id','=',$id)
        ->get();
        return view('admin.procurement.show')
        ->with('menu','procurement')
        ->with('procurement',$procurement)
        ->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Procurement  $procurement
     * @return \Illuminate\Http\Response
     */
    public function edit(Procurement $procurement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Procurement  $procurement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Procurement $procurement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Procurement  $procurement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Procurement $procurement)
    {
        //
    }

    public function accept($id)
    {
        //
        $data = Procurement::find($id);
        if($data->status == 'applied'){
            $data->status = 'accept1';
        }elseif($data->status == 'accept1'){
            $data->status = 'accept2';
        }else{
            $data->status = 'accept3';
        }
        $data->save();
        return redirect('/procurement');
    }

    public function fund($id)
    {
        //
        $data = DB::table('procurements')
        ->join('users','users.id','=','procurements.applicant')
        ->join('occupations','users.occupation','=','occupations.id')
        ->join('divisions','users.division','=','divisions.id')
        ->select('procurements.*','users.name','divisions.name as divname','occupations.name as occuname')
        ->get()->first();
        return view('admin.procurement.fund')
        ->with('menu','procurement')
        ->with('data',$data);
    }

    public function cart(){
        
     $data = DB::table('procurements')
        ->join('users','users.id','=','procurements.applicant')
        ->join('occupations','occupations.id','=', 'users.occupation')
        ->join('divisions','divisions.id','=','occupations.division_id')
        ->select('procurements.*','users.name','divisions.name as division')
        ->get();
        return view('admin.procurement.index')
        ->with('menu','procurement')
        ->with('data',$data);
    }

    public function changeStatus() 
    {
        $id = Input::get('id');

        $post = Post::findOrFail($id);
        $post->is_published = !$post->is_published;
        $post->save();

        return response()->json($post);
    }

     public function addItem($id,$jml) 
    {
        $data= Item::find($id);
   //     $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
     //       return $cartItem->id === $request->id;
       // });

        //if (!$duplicates->isEmpty()) {
          //  return redirect('cart')->withSuccessMessage('Item is already in your cart!');
 //       }
         Cart::add($id, $data->name, $jml, $data->price);
         return response()->json(['success' => true]);
    }
}
