<?php

namespace App\Http\Controllers;

use App\User;
use App\Occupation;
use App\Division;
use App\ItemCategory;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Alert;

class ItemCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $data = ItemCategory::all();
        return view('admin.itemcategory.index')
        ->with('menu', 'item')
        ->with('data', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.itemcategory.add')
        ->with('menu','item');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new ItemCategory;
        $data->name = $request->name;
        $data->code = $request->code;
        $data->save();

        Alert::success('Data Kategori Barang berhasil Ditambahkan', 'Berhasil')->autoclose(2000);
        return redirect('/itemcategory')->with('menu','item');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ItemCategory $itemCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ItemCategory::find($id);
             return view('admin.itemcategory.edit')
        ->with('menu','item')
        ->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ItemCategory::find($id);
        $data->name = $request->name;
        $data->code = $request->code;
        $data->save();

        Alert::success('Data Kategori Barang berhasil Diubah', 'Berhasil')->autoclose(2000);
        return redirect('/itemcategory')->with('menu','item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemCategory  $itemCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           $data = ItemCategory::find($id);
        $data->delete();

        Alert::success('Data Kategori Barang berhasil Dihapus', 'Berhasil')->autoclose(2000);
        return redirect('/itemcategory')->with('menu','item');
    }
}
