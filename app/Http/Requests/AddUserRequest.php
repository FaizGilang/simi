<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            //
            'name' => 'required',
            'email' => 'email|required|unique:users,email',
            'occupation' => 'required',
        ];
        
    }
    public function messages()
    {
        
        return [
           
            'name.required' => 'Nama Pengguna Tidak Boleh Kosong',
            'email.required' => 'Email Tidak Boleh Kosong',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email sudah dipakai',
            'occupation.required' => 'Jabatan tidak boleh kosong',
        ];
    }
}


