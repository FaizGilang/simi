<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOccupationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'name' => 'required',
            'occupation' => 'required',
        ];
        
    }
    public function messages()
    {
        
        return [
           
            'name.required' => 'Nama Jabatan Tidak Boleh Kosong',
            'occupation.required' => 'Jabatan Tidak Boleh Kosong',
        ];
    }
}
