<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            //
            'name' => 'required',
            'category' => 'required',
            'specification' => 'required',
            'code' => 'required',
        ];
        
    }
    public function messages()
    {
        
        return [
             'name.required' => 'Nama Barang Tidak Boleh Kosong',
             'category.required' => 'Kategori Tidak Boleh Kosong',
             'specification.required' => 'Spesifikasi Tidak Boleh Kosong',
             'code.required' => 'Code Tidak Boleh Kosong',
        ];
    }
}
