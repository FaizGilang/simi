<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddArrivalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'year' => 'required',
            'vendor' => 'required',
            'field_name' => 'required',
            'field_source' => 'required',
            'field_amount' => 'required',
        ];
        
    }
    public function messages()
    {
        
        return [
              
            'year.required' => 'Tahun Pengadaan Tidak Boleh Kosong',
            'vendor.required' => 'Vendor Tidak Boleh Kosong',
            'field_name.required' => 'Barang Tidak Boleh Kosong',
            'field_source.required' => 'Sumber Dana Divisi Tidak Boleh Kosong',
            'field_amount.required' => 'Jumlah Tidak Boleh Kosong',
        ];
    }
}
